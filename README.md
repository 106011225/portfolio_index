# portfolio_index

- [x] [Operating system](https://gitlab.com/106011225/portfolio_index#operating-system)
- [x] [Parallel computing](https://gitlab.com/106011225/portfolio_index#parallel-computing)
- [x] [Compiler](https://gitlab.com/106011225/portfolio_index#compiler)
- [x] [Logic design (Verilog, FPGA)](https://gitlab.com/106011225/portfolio_index#logic-design-verilog-fpga)
- [x] [Machine learning](https://gitlab.com/106011225/portfolio_index#machine-learning)
- [x] [Massive data analysis (PySpark)](https://gitlab.com/106011225/portfolio_index#massive-data-analysis-pyspark)
- [ ] [Web programming](https://gitlab.com/106011225/portfolio_index#web-programming)
- [ ] [Other repositories](https://gitlab.com/106011225/portfolio_index#other-repositories)


## Operating system  

- *NachOS* assignments 
    - [System call](https://gitlab.com/106011225/os_mp1_2020)
    - [Multi-programming](https://gitlab.com/106011225/os_mp2_2020)
    - [CPU scheduling](https://gitlab.com/106011225/os_mp3_2020)
    - [File system](https://gitlab.com/106011225/os_mp4_2020)

> *NachOS* is instructional software for teaching undergraduate, and potentially graduate, level operating systems courses.  
> Website: https://homes.cs.washington.edu/~tom/nachos/

- Linux kernel assignments
    - [Character devices](https://gitlab.com/106011225/linux_kernel_assignments/-/tree/main/Character%20devices)
    - [File system](https://gitlab.com/106011225/linux_kernel_assignments/-/tree/main/File%20system)
    - [System call](https://gitlab.com/106011225/linux_kernel_assignments/-/tree/main/System%20call)
    - [Modify the memory of a process](https://gitlab.com/106011225/linux_kernel_assignments/-/tree/main/Modify%20the%20memory%20of%20a%20process)


## Parallel computing

- [Labs](https://gitlab.com/106011225/i2pc_labs)
- [A sokoban game solver with **OpenMP**](https://gitlab.com/106011225/i2pc_hw1)
- [Mandelbulb visualization with **MPI** and **OpenMP**](https://gitlab.com/106011225/i2pc_hw2)
- [Edge detection with **CUDA**](https://gitlab.com/106011225/i2pc_hw3)
- [Bitcoin miner with **CUDA**](https://gitlab.com/106011225/i2pc_hw4)
- [N-body simulation with **CUDA**](https://gitlab.com/106011225/i2pc_hw5)
- [PageRank with **CUDA**](https://gitlab.com/106011225/i2pc_final_project)


## Compiler

- [Lexical analyzer with `Flex`](https://gitlab.com/106011225/compiler_hw1)
- [Syntax analyzer with `Yacc`](https://gitlab.com/106011225/compiler_hw2)
- [Code generation](https://gitlab.com/106011225/compiler_hw3)
- [Data dependence analysis with LLVM passes](https://gitlab.com/106011225/advanced_compiler_assignments/-/tree/main/AC_hw1)
- [Pointer analysis with LLVM passes](https://gitlab.com/106011225/advanced_compiler_assignments/-/tree/main/AC_hw2)


## Logic design (Verilog, FPGA)

- [ALU designs](https://gitlab.com/106011225/ld_labs/-/tree/main/lab1)
- [Normal counter, Fibonacci counter, Gray code counter](https://gitlab.com/106011225/ld_labs/-/tree/main/lab2)
- [Clock divider and **LED** controller](https://gitlab.com/106011225/ld_labs/-/tree/main/lab3)
- [Counters and **7-segment display**](https://gitlab.com/106011225/ld_labs/-/tree/main/lab4)
- [The fancy mask vending machine (**7-segment display**, **push buttons**)](https://gitlab.com/106011225/ld_labs/-/tree/main/lab5)
- [Theme park monorail (**keyboard**, **7-segment display**, **push buttons**, **LED**)](https://gitlab.com/106011225/ld_labs/-/tree/main/lab6)
- [Digital photo frame (**VGA**)](https://gitlab.com/106011225/ld_labs/-/tree/main/lab7)
- [Music player (**audio peripheral**, **7-segment display**, **push buttons**, **LED**)](https://gitlab.com/106011225/ld_labs/-/tree/main/lab8)
- [The connect four game (**VGA**, **keyboard**, **7-segment display**, **push buttons**, **LED**)](https://gitlab.com/106011225/ldlab2020_final_project)


## Machine learning

- [[Computer Vision] An approach to representing images as grid-based implicit functions with
controllable level of details](https://gitlab.com/106011225/cvdlproj1/-/tree/main/Main_project)
- [[Computer Vision] NeRF with merged training data](https://gitlab.com/106011225/cvdlproj1/-/tree/main/NeRF_with_merged_training_data)
- [COVID-19 forecast (new cases per day)](https://gitlab.com/106011225/i2ml_assignments/-/tree/main/I2ML_hw1)
- [COVID-19 30-day mortality prediction](https://gitlab.com/106011225/i2ml_assignments/-/tree/main/I2ML_hw2)
- [COVID-19 30-day mortality prediction from CXR](https://gitlab.com/106011225/i2ml_assignments/-/tree/main/I2ML_hw3)


## Massive data analysis (PySpark)

- [Matrix multiplication](https://gitlab.com/106011225/massive_data_analysis_assignments/-/tree/main/MDA_hw1)
- [PageRank](https://gitlab.com/106011225/massive_data_analysis_assignments/-/tree/main/MDA_hw2)
- [K-means](https://gitlab.com/106011225/massive_data_analysis_assignments/-/tree/main/MDA_hw3)
- [Recommendation system: item-item collaborative filtering](https://gitlab.com/106011225/massive_data_analysis_assignments/-/tree/main/MDA_term_proj)


## Web programming

- [Labs]()
- [WebCanvas]()
- [Raiden (a simple aircraft game)]()
- [Omazon (a simple shopping web page)]()
- [Plants vs Zombies simple (a simple tower defense game)]()


## Other repositories

- [[Cryptography] Substitution cipher breaker (Python)]()
- [[C/C++] Simple mini compiler]()
- [[C/C++] Unlimited craft works (a simple aircraft game)]()
- [[C/C++] Data structure assignments]()
- [[Computational geometry] Presentation slides (topic: intersection of convex polygons, star-shaped polygons and half-planes)](https://gitlab.com/106011225/computational_geometry_presentation_slides)
